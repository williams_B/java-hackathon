public class Pig extends Animal{
    
    //private String name;
    //private String sound;

    public Pig() {
        this.name = "Pig";
        this.sound = "Oink Oink";
    }

    public Pig(String nm) {
        this.name = nm;
        this.sound = "Oink Oink";
    }

}