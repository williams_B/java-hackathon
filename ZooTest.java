import java.util.ArrayList;

public class ZooTest {

    public static void main(String[] args) {
        ArrayList<String> animals = new ArrayList<String>();
        String[] rabbitNames = {"Chester", null, "Bunny", "Carrot"};
        String[] pigNames = {"Charlotte", "Pink"};
        String[] catNames = {null, null, "Spot"};
        int i = 0;
        
        while (i < 20) {
            String name = "";
            if (i < rabbitNames.length) {
                name  = rabbitNames[i];
                if (name != null) {
                    Rabbit aRabbit = new Rabbit(name);
                    //System.out.println(name);
                    animals.add(aRabbit.getName());
                } else {
                    Rabbit aRabbit = new Rabbit();
                    animals.add(aRabbit.getName());
                }
            }

            if (i < pigNames.length) {
                name  = pigNames[i];
                if (name != null) {
                    Pig aPig = new Pig(name);
                    //System.out.println(name);
                    animals.add(aPig.getName());
                } else {
                    Pig aPig = new Pig();
                    animals.add(aPig.getName());
                }
            }

            if (i < catNames.length) {
                name  = catNames[i];
                if (name != null) {
                    Cat aCat = new Cat(name);
                    //System.out.println(name);
                    animals.add(aCat.getName());
                } else {
                    Cat aCat = new Cat();
                    animals.add(aCat.getName());
                }
            }
            i++;
        }


        /*for (String name: rabbitNames) {
            if (name != null) {
                Rabbit aRabbit = new Rabbit(name);
                System.out.println(aRabbit.getName());
            } else {
                Rabbit aRabbit = new Rabbit();
                System.out.println(aRabbit.getName());
            } 
        } */
        System.out.println(animals);
    }
}