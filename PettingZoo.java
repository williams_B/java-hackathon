import java.util.ArrayList;

public class PettingZoo {

    private Object[] theStacks;
    private Object[] haveOut;

    public PettingZoo() {
        theStacks = new Object[50];
        haveOut = new Object[20];
    }

    // Add an animal to pet in the zoo
    public void addAnimal(Object a) {
        int tCount = 0;
        while (tCount < 100) {
            if (theStacks[tCount] == null) {
                theStacks[tCount] = a;
                break;
            }
            tCount++;
        }
    }
    // If an animal is in the petting zoo t
    public boolean isAvailable(Object x) {
        boolean ready = false;
        for (int i = 0; i < theStacks.length; i++) {
            Object animal = theStacks[i];
            if (animal == x) {
                ready = true;
                break;
            }
        }
        return ready;
    }

    // If a particular animal is in the zoo
    public boolean isAvailable(String t) {
        boolean ready = false;
        for (int i = 0; i < theStacks.length; i++) {
            Object animal = theStacks[i];
            String name = animal.getName();
            if (name == t) {
                ready = true;
                break;
            }
        }
        return ready;
    }

    // If someone is taking an animal out of the petting zoo
    public boolean borrowing() {
        boolean gone = false; // Tells us if we were able to take it out
        int hCount = 0;
        
        while (hCount < theStacks.length) {
            if (theStacks[hCount] != null) {
                Object animal = theStacks[hCount];
                for (int j = 0; j < haveOut.length; j++) {
                    if (haveOut[j] == null) {
                        haveOut[j] = animal; // Place in borrowed list
                        gone = true;
                        theStacks[hCount] = null; // Take out from animal list
                        break;
                    }
                }
                if (gone != true) {  // Can't take out anymore animals 
                    break; 
                }
            }
            hCount++;
        }
        return gone;
    }

    // Borrowing a particular animal
    public boolean borrowing(Object y) {
        boolean gone = false; // Can't take out the animal
        for (int i = 0; i < theStacks.length; i++) {
            Object animal = theStacks[i];
            if (animal == y) {
                for (int j = 0; j < haveOut.length; j++) {
                    if (haveOut[j] == null) {
                        haveOut[j] = animal;
                        theStacks[i] = null; // take out from animal list
                        gone = true;
                        break;
                    }
                }
                //if (gone == false) { // If we weren't able to borrow the animal
                    //break; 
                //}
            } 
        }
        return gone;
    }

    // If someone is putting an animal back in the petting zoo
    public boolean returning(Object z) {
        boolean back = false;
        int hCount = 0;

        while (hCount < haveOut.length) {
            if (haveOut[hCount] == z) {
                Object animal = haveOut[hCount];
                for (int j = 0; j < theStacks.length; j++) {
                    if (theStacks[j] == null) {
                        theStacks[j] = animal;
                        back = true;
                        break;
                    }
                }
                if (back == true) {
                    break;
                }
            }
            hCount++; 
        }
        return back;
    }
    
}